from flask import request, Response
from flask_api import FlaskAPI
from flask_cors import CORS
import csv
import json
import papermill as pm
import os


app = FlaskAPI(__name__)
# enable cross origin resource sharing
CORS(app)

my_config = app.config.from_pyfile(os.path.join('.', 'config/app.conf'), silent=False)


def json_data(filename):
    json_data = [json.dumps(d) for d in csv.DictReader(open(filename))]
    return json_data


@app.route("/model/cnn", methods=['GET'])
def model():
    input_file=app.config.get("CNN_INPUT_FILE")
    output_file=app.config.get("CNN_OUTPUT_FILE")
    model=app.config.get("CNN_MODEL")
    pickle=app.config.get("CNN_PICKLE")
    results=app.config.get("CNN_RESULTS")
    input_data=app.config.get("CNN_INPUT_DATA")

    pm.execute_notebook(input_file, output_file, parameters={"cnn_model":model, "cnn_pkl_file":pickle, "cnn_results":results, "cnn_input_data":input_data})
    return json_data(results)
    
	
@app.route("/models", methods=['GET'])
def get_model():
    type = request.args.get('type')
    metric = request.args.get('metric')
    start_date = request.args.get('start_date')
    periods = request.args.get('periods')

    if type.casefold() == 'hotel' and metric.casefold() == 'cnn':
        print('in if')
        input_file=app.config.get("CNN_INPUT_FILE")
        output_file=app.config.get("CNN_OUTPUT_FILE")
        model=app.config.get("CNN_MODEL")
        pickle=app.config.get("CNN_PICKLE")
        results=app.config.get("CNN_RESULTS")
        input_data=app.config.get("CNN_INPUT_DATA")
		
        pm.execute_notebook(input_file, output_file, parameters={"cnn_model":model, "cnn_pkl_file":pickle, "cnn_results":results, "cnn_input_data":input_data, "start_date":start_date, "periods":periods})
        return json_data(results)
    else:
        return 'No data found.'


@app.route("/model", methods=['POST'])
def execute_model():
    request_json = request.get_json()

    type = request_json.get('type')
    metric = request_json.get('metric')
    start_date = request_json.get('start_date')
    periods = request_json.get('periods')

    if type == 'hotel' and metric.casefold() == 'cnn':
        input_file=app.config.get("CNN_INPUT_FILE")
        output_file=app.config.get("CNN_OUTPUT_FILE")
        model=app.config.get("CNN_MODEL")
        pickle=app.config.get("CNN_PICKLE")
        results=app.config.get("CNN_RESULTS")
        input_data=app.config.get("CNN_INPUT_DATA")

        pm.execute_notebook(input_file, output_file, parameters={"cnn_model":model, "cnn_pkl_file":pickle, "cnn_results":results, "cnn_input_data":input_data, "start_date":start_date, "periods":periods})
        return json_data(results)


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8080, debug=True)